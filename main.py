import random as rnd
import haravasto as hv
import time
import sys
import os
from menu import tee_paavalikko, lopetus_valikko
from datetime import datetime

tila = {
        "kentta_nakyva": [],
        "kentta_piilo": [],

        }

tilastot = {
    "aika": 0,
    "klikkaukset": 0,
    "paivamaara": datetime.now().strftime("%d/%m/%Y %H:%M:%S")
}

def luo_kentta(leveys1, korkeus1):
    """
    Luo peli kentan eli listan jonka sisalla on listoja

        :param int leveys: Määrää kentän leveyden
        :param int korkeus: Määrää kentän korkeuden
    """
    kentta = []
    for i in range(korkeus1):
        kentta.append([])
        for n in range(leveys1):
            kentta[-1].append(" ")

    jaljella = []
    for x in range(leveys1):
        for y in range(korkeus1):
            jaljella.append((x, y))

    return kentta, jaljella

def miinoita(alue, vapaat_ruudut, miinat):
    """
    Asettaa n kappaletta miinojen pelikentälle.

        :param list alue: lista mille miinat laitetaan

        :param list vapaat_ruudut:
            lista, joka pitää sisällään vapaat koordinaatti parit

        :param int miinat: Miinojen määrä
    """
    for i in range(miinat):
        satun_ruutu = rnd.choice(vapaat_ruudut)
        x_koord, y_koord = satun_ruutu
        alue[y_koord][x_koord] = "x"
        vapaat_ruudut.remove(satun_ruutu)

def piirto_kasittelija():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    hv.tyhjaa_ikkuna()
    hv.aloita_ruutujen_piirto()

    for kerroin_y, ruutu in enumerate(tila["kentta_nakyva"]):
        y_koord = kerroin_y * 40 #yksi ruutu on kooltaan 40 * 40 pikseliä
        for kerroin_x, avain in enumerate(ruutu):
            x_koord = kerroin_x * 40
            hv.lisaa_piirrettava_ruutu(avain, x_koord, y_koord)

    hv.piirra_ruudut()

def laske_miinat(lista, x, y):
    """
    Laskee ruudun ympärillä olevien miinojen määrän
        :param list lista: lista johon miinat on merkattu
        :param int x: ruudun x koordinaatti
        :param int y: ruudun y koordinaatti
    """
    miinat = 0
    for i in lista[max(y-1, 0): y+2]:
        for n in i[max(x-1, 0): x+2]:
            if n == "x":
                miinat = miinat + 1

    return miinat

def piilo_kasittely(lista):
    """
    Pelaa pelin läpi ns piilokentällä
        :param list lista: lista joka pitää sisällään piilokentän arvot
    """
    for i, rivi in enumerate(lista):
        for n, sarake in enumerate(rivi):
            if sarake != "x":
                lista[i][n] = str(laske_miinat(lista, n, i))

def kirjoita_tilastoon(kohde):
    try:
        with open(kohde, "a") as tiedosto:
            aika = tilastot["aika"]
            paiva = tilastot["paivamaara"]
            vuorot = tilastot["klikkaukset"]
            mins, sec = divmod(aika, 60)
            tulos = "Voitto" if not vapaat else "Häviö"
            tiedosto.write("Päivämäärä: {paiva}\nAika: {mins} min {s} s\nVuorojen määrä: {vuorot}\nTulos: {tulos}\nMitat: {leveys}x{korkeus} ja {miinat} miinaa\n\n".format(
                                paiva=paiva, mins=mins, s=sec,
                                leveys=leveys, korkeus=korkeus,
                                miinat=miinat, vuorot=vuorot, tulos=tulos))
    except IOError:
        print("Tiedoston avaaminen epäonnistui")

def avaa_tilastot():
    with open("tilastot.txt") as tiedosto:
        if sys.platform == "win32":
            os.system("start " + "tilastot.txt")

        else:
            os.system("vim tilastot.txt &")

def tulvataytto(lista1, lista2, x, y):
    """Sisältää lopetus ehdot, sekä kentän täyttämisen"""
    if (x, y) in vapaat:
        tilastot["klikkaukset"] += 1

    if lista1[y][x] == "x":
        # Nämä for loopit eivät ole tarpeellisia
        # Koodin parantamisen kannalta tärkeitä
        for i, rivi in enumerate(lista1):
            for n, sarake in enumerate(rivi):
                if sarake == "x":
                    lista2[i][n] = lista1[i][n]
        tilastot["klikkaukset"] += 1
        hv.lopeta()

    else:
        #print(vapaat)
        alue = [(x, y)]
        while alue:
           # print(alue)
            xk, yk = alue.pop(-1)

            lista2[yk][xk] = lista1[yk][xk]
            if lista1[yk][xk] != "0" and lista1[yk][xk] !="x":
                lista2[yk][xk] = lista1[yk][xk]

                # Tälläistä ehto-lausetta ei pitäisi tarvita aiempien ehtojen
                # perusteella, mutta silti syntyy error jonka korjaamiseen
                # tätä ehtoa tarvitaan. Koskee kaikki tämän tapaisia tapauksia
                if (xk, yk) in vapaat:
                    vapaat.remove((xk, yk))

                #if not vapaat:
                #     hv.lopeta

                continue

            for i in range(yk-1, yk+2):
                for n in range(xk-1, xk+2):
                    if (i >= 0 and i <= len(lista1)-1) and (n >= 0 and n <= len(lista1[i])-1):

                        if (lista2[i][n] == " " or lista2[i][n] == "f") and (lista1[i][n] != "x"):
                            alue.append((n, i))

                            if (n, i) in vapaat:
                                vapaat.remove((n, i))

                            if (xk, yk) in vapaat:
                                vapaat.remove((xk, yk))

                           # if not vapaat:
                           #     hv.lopeta()
        #print(vapaat)
def liputa(lista, x, y):
    """Asettaa lipun määrättyyn koordinaattiin"""
    if lista[y][x] == "f":
        lista[y][x] = " "

    elif lista[y][x] != "f" and lista[y][x] == " ":
        lista[y][x] = "f"

def hiiri_kasitelija(x, y, nappi, muokkausnappi):
    """Tarvitaan haravastoa varten katso lisä-infoa haravaston help sivuilta"""
    if nappi == hv.HIIRI_VASEN:
        tulvataytto(tila["kentta_piilo"], tila["kentta_nakyva"], int(x/40), int(y/40))

        if not vapaat:
            hv.lopeta()

    if nappi == hv.HIIRI_OIKEA:
        liputa(tila["kentta_nakyva"], int(x/40), int(y/40))

def ajastin(n):
    tilastot["aika"] += 1

def main():
    """Ohjelman main funktio"""
    #leveys, korkeus = tee_paavalikko()
    tila["kentta_nakyva"], _ = luo_kentta(leveys, korkeus)
    tila["kentta_piilo"], _ = luo_kentta(leveys, korkeus)
    nakyva = tila["kentta_nakyva"]
    piilo = tila["kentta_piilo"]
    leveys_px = len(nakyva[0]) * 40
    korkeus_px = len(nakyva) * 40

    miinoita(piilo, vapaat, miinat)
    piilo_kasittely(piilo)
    hv.lataa_kuvat("spritet")
    hv.luo_ikkuna(leveys_px, korkeus_px)
    hv.aseta_toistuva_kasittelija(ajastin, toistovali=1)
    hv.aseta_piirto_kasittelija(piirto_kasittelija)
    hv.aseta_hiiri_kasittelija(hiiri_kasitelija)
    hv.aloita()
    kirjoita_tilastoon("tilastot.txt")
    lopetus_valikko(vapaat)

if __name__ == "__main__":
    while True:
        try:
            leveys, korkeus, miinat = tee_paavalikko(avaa_tilastot)
            tilastot["aika"] = 0
            tilastot["klikkaukset"] = 0

        except TypeError:
            break

        else:
            _, vapaat = luo_kentta(leveys, korkeus)
            main()
