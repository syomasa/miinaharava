Ohjelmoinin alkeiden lopputyö

***ASENNUS***
1. Pelin suorittamiseen tarvitset python 3.\*\* 
    ohjeet asennukseen löydät osoitteesta https://www.python.org/
    
2. Pythonia asentaessa muista lisätä se **PATH** ja valitse custom instalation tms.

3. Muista asentaa python kaikille käyttäjille

4. Onnistuneen asennuksen jälkeen avaa terminaali järjestelmän valvojana

5. Tämän jälkeen kirjoita: **pip install pyglet**

6. Tämän jälkeen voit suorittaa ohjelman suorittamalla **main.py** tiedoston miinaharava kansiossa