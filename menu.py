from tkinter import *
import json
import sys
from datetime import datetime

def lataa_config():
    """
    Ottaa config.json tiedostossa määritetyt arvot käyttöön
    config.json tiedoston tulee olla samassa kansiossa kuin
    tiedosto, joka käyttää functiota.
    """
    try:
        with open("config.json", "r") as config:
            tiedosto = json.load(config)

    except (IOError, json.JSONDecodeError):
        print("Config tiedoa ei voitu ladata")

    return tiedosto

def luo_iso_painike(piirto_kohde, komento,
                    asetus = lataa_config(), teksti = "Aloita peli"):
    """
    Luo aloitus painikkeen
        :param func piirto_kohde: ikkuna johon painike luodaan
        :param func komento: Komento, jonka painike suorittaa painettaessa

        :param func asetus:
            Määritää menun teeman olisi suotavaa parametrin
            default arvon muuntamisen sijaan muokattaisiin
            config.json tiedoston arvoja.

    """

    tiedot = asetus["luo_valikko"]["painike"]

    aloitus_painike = Button(
        piirto_kohde,
        width=tiedot["width"], height=tiedot["height"],
        font=tiedot["font"], text=teksti,
        bg=tiedot["bg"], command=komento)

    return aloitus_painike

def luo_syote_kentta(piirto_kohde):
    """Luo syote kentan"""
    leveys = Entry(piirto_kohde, width="40")
    korkeus = Entry(piirto_kohde, width="40")
    miinat = Entry(piirto_kohde, width="40")

    return leveys, korkeus, miinat

def lopetus_valikko(jaljella):
    "Valikko joka ilmestyy pelin päättyessä"
    lopetus = Tk()
    lopetus.geometry("500x250")
    lopetus.title("Uusi peli?")
    voitto = Label(lopetus, text="Voitit pelin!", font=("Mono Sans", 35))
    havio = Label(lopetus, text="Hävisit pelin!", font=("Mono Sans", 35))
    uusi = Button(lopetus, text="Uusi peli", font=("Mono Sans", 20), command=lopetus.destroy)
    lopeta = Button(lopetus, text="Lopeta peli", font=("Mono Sans", 20), command=sys.exit)
    uusi.place(x=75, y=180)
    lopeta.place(x=300, y=180)

    if not jaljella:
        voitto.place(x=120, y=80)
    else:
        havio.place(x=120, y=80)

    lopetus.mainloop()

def luo_valikko(piirto, komento1, komento2, komento3):
    """
    Luo päävalikko elementit
        :param func piirto: valikkoon lisättävien elementien kohde
        :param func komento1: Aloita peli painikkeen suorittama komento
        :param func komento2: Tilastot painikkeen suorittama komento
    """
    piirto.geometry("450x600")
    piirto.title("Miinaharava")

    syotteet = luo_syote_kentta(piirto)
    for i, syote in enumerate(syotteet, 1):
        syote.place(x=80, y=100*i)

    Label(piirto, text="Anna kentän leveys ruutuina.").place(x=80, y=70)
    Label(piirto, text="Anna kentän korkeus ruutuina.").place(x=80, y=170)
    Label(piirto, text="Anna miinojen lukumäärä").place(x=80, y=270)

    luo_iso_painike(piirto, komento1).place(x=80, y=360)
    luo_iso_painike(piirto, komento2, teksti="Tilastot").place(x=80, y=420)
    luo_iso_painike(piirto, komento3, teksti="Lopeta").place(x=80, y=480)

    piirto.mainloop()
    return syotteet

def kasittele_valikko(piirto, valikko):
    """
    Kasittelee valikkon teksti kenttien syötteitä, muuntaa ne kokonaisluvuiksi
    ja palauttaa ne
        :param func piirto: Pääikkuna josta virhe ikkuna on riippuvainen
        :param func valikko: Valikko, jonka teksti kentistä arvot saadaan
    """
    while True:
        try:
            leveys_syot, korkeus_syot, miinat_syot = valikko
            leveys = int(leveys_syot.get())
            korkeus = int(korkeus_syot.get())
            miinat = int(miinat_syot.get())

            if miinat > korkeus*leveys:
                error = Toplevel(piirto, width="400", height="150")
                error_text = Label(
                        error, text="Liian paljon miinoja", font="bold")
                error_text.place(x=80, y=50)
                Button(error, text="Ok", command=error.destroy).place(x=80, y=100)
                error.mainloop()
                continue

        except ValueError:
            top = Toplevel(piirto, width="400", height="150")
            virhe_text =  Label(
                    top, text="Vain kokonaislukuja, kiitos.", font="bold")
            virhe_text.place(x=80, y=50)

            Button(top, text="Ok", command=top.destroy).place(x=80, y=100)
            top.mainloop()

        except TclError:
            return

        else:
            return leveys, korkeus, miinat

def tee_paavalikko(komento):
    """Tekee päävalikon komento parametri on tilasto painikketta varten"""
    try:
        naytto = Tk()
        leveys, korkeus, miinat = kasittele_valikko(
               naytto, luo_valikko(naytto, naytto.quit, komento, sys.exit))

    except TypeError:
        return

    else:
        naytto.destroy()
        return leveys, korkeus, miinat
